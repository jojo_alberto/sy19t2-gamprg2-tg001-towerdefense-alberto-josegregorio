// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Gamprog2TowerDefenseTarget : TargetRules
{
	public Gamprog2TowerDefenseTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Gamprog2TowerDefense" } );
	}
}
