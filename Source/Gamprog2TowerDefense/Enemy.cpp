// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "Engine/World.h"
#include "EnemyAIController.h"
#include "Kismet/GameplayStatics.h"
#include "GameModeBaseClass.h"
#include "HP.h"
#include "Myplayer.h"
#include "Kismet/GameplayStatics.h"
#include "WayPoint.h"


// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	EnemyHealth = CreateDefaultSubobject<UHP>(TEXT("Health Component"));

}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	EnemyHealth->CharDie.AddDynamic(this, &AEnemy::goldGain);
	EnemyHealth->CharDie.AddDynamic(this,&AEnemy::Destroyer);
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(this,AWayPoint::StaticClass(),Waypoints);
	//MoveToWaypoint();
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::MoveToWaypoint()
{
	AEnemyAIController* EnemyAIController = Cast<AEnemyAIController>(GetController());

	if (EnemyAIController)
	{
		if (currrentWayPoint >= Waypoints.Num())
		{
			
			AMyplayer* player = Cast<AMyplayer>(UGameplayStatics::GetPlayerPawn(GetWorld(),0));
			player->Health->TakeDamage(1);
			
			Destroyer();
			return;
		}
		
		if (currrentWayPoint <= Waypoints.Num() - 1)
		{


			//AWayPoint* waypoitDetector = Cast<AWayPoint>(Waypoints[currrentWayPoint]);
			EnemyAIController->MoveToActor(Waypoints[currrentWayPoint], 5.0f, false);
			currrentWayPoint++;

		}

	}
}

void AEnemy::Destroyer()
{

	Destroy();
}

void AEnemy::goldGain()
{

	AMyplayer* playerCash = Cast<AMyplayer>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	playerCash->playerMoney += reward;
}

