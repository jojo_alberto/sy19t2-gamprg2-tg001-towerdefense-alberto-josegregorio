// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

UCLASS()
class GAMPROG2TOWERDEFENSE_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void MoveToWaypoint();
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UHP * EnemyHealth;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float reward = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int currrentWayPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AActor*> Waypoints;
	UFUNCTION(BlueprintCallable)
	void Destroyer();
	UFUNCTION(BlueprintCallable)
	void goldGain();

};
