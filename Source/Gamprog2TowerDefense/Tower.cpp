// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Enemy.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "TimerManager.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Ammo.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (currentTarget == NULL)
	{
		DetectEnemy();
	}
	else if(canAttack == true)
	{		
		Attack();
	}
}

void ATower::Attack()
{
	FVector targetDir;
	if (currentTarget == NULL)
	{
		GetWorld()->GetTimerManager().ClearTimer(AttackRateHandle);
	}
	GetWorld()->GetTimerManager().SetTimer(AttackRateHandle, this, &ATower::attackCD, AttackRate, false);
	//GetWorld()->SpawnActor<AAmmo>(Projectile,GetActorLocation(), GetActorRotation());
	AAmmo* spawnedAmmo = GetWorld()->SpawnActor<AAmmo>(Projectile, GetActorLocation(), GetActorRotation());
	UProjectileMovementComponent * ProjComponent = spawnedAmmo->bullet;
	targetDir = currentTarget->GetActorLocation() - GetActorLocation();
	targetDir = UKismetMathLibrary::Normal(targetDir);
	ProjComponent->Velocity = targetDir * spawnedAmmo->speed;
	canAttack = false;

}

void ATower::DetectEnemy()
{

	if (currentTarget != NULL)
	{
		return;
	}

	TArray<FHitResult> Outhits;
	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Add(this);
	UKismetSystemLibrary::SphereTraceMultiForObjects(this, GetActorLocation(), GetActorLocation(), RadiusDetection, DetectionObjectQuery, false, ActorsToIgnore, EDrawDebugTrace::ForOneFrame, Outhits, true);

	float closestEnemy = 10000000;
	AEnemy* Target;

	for (auto i : Outhits)
	{
		if (i.Distance < closestEnemy)
		{
			Target = Cast<AEnemy>(i.Actor);
			if (Target == NULL)
			{
				continue;
			}
			currentTarget = Target;
			closestEnemy = i.Distance;
		}


		
	}

}

void ATower::attackCD()
{
	canAttack = true;
	if (currentTarget == NULL)
	{
		return;
	}
	if (FVector::Distance(currentTarget->GetActorLocation(), GetActorLocation())>RadiusDetection)
	{
		
		currentTarget = NULL;

	}
}

