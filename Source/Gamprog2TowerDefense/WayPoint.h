// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "WayPoint.generated.h"

/**
 * 
 */
UCLASS()
class GAMPROG2TOWERDEFENSE_API AWayPoint : public AStaticMeshActor
{
	GENERATED_BODY()
public:
	int getWayPoint();


private:
	UPROPERTY(EditAnywhere,meta =(AllowPrivateAccess = "true"))
	int WayPoint;
};
