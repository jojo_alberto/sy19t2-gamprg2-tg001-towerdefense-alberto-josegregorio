// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerHolder.generated.h"

UCLASS()
class GAMPROG2TOWERDEFENSE_API ATowerHolder : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerHolder();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	FVector TowerLocation();
	virtual FVector TowerLocation_Implementation();




protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
