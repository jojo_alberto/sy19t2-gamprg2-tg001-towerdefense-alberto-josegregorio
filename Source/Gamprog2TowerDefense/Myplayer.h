// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Myplayer.generated.h"

UCLASS()
class GAMPROG2TOWERDEFENSE_API AMyplayer : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyplayer();
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UHP * Health;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UFUNCTION(BlueprintCallable)
	void CanBuild();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class ATower * BuildTarget;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ECollisionChannel> DetectionObjectQuery;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class ATower> Tower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ETraceTypeQuery> TraceChannel;
	FVector buildableSpot;
	UFUNCTION(BlueprintCallable)
	void ghostTower();
	FHitResult hitResult;
	bool isOnValidLoc = false;
	UFUNCTION(BlueprintCallable)
	void PlaceTower();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector tempStorage;

	bool isOnBuild = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float playerMoney;
	UFUNCTION(BlueprintCallable)
	void StartBldgTower(TSubclassOf<class ATower> newTower,float Cost);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float currentCost;


};
