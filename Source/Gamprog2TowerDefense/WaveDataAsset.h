// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Enemy.h"
#include "WaveDataAsset.generated.h"

USTRUCT(BlueprintType)
struct FenemyWave
{
	GENERATED_USTRUCT_BODY()
public:
	FenemyWave()
	{}
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AEnemy> EnemyType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Count;
};



/**
 * 
 */
UCLASS()
class GAMPROG2TOWERDEFENSE_API UWaveDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FenemyWave> enemyWave;
};
