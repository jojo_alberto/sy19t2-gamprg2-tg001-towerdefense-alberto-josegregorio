// Fill out your copyright notice in the Description page of Project Settings.

#include "Myplayer.h"
#include "Engine.h"
#include "Tower.h"
#include "TowerHolder.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "HP.h"

// Sets default values
AMyplayer::AMyplayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Health = CreateDefaultSubobject<UHP>(TEXT("Health Component"));


}

// Called when the game starts or when spawned
void AMyplayer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyplayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (isOnBuild == false)
	{
		return;
	}

	CanBuild();
	if (BuildTarget != NULL)
	{
		ghostTower();
	}

}

// Called to bind functionality to input
void AMyplayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("PlaceTower", IE_Pressed, this, &AMyplayer::PlaceTower);

}

void AMyplayer::StartBldgTower(TSubclassOf<class ATower> newTower, float Cost)
{
	if (playerMoney < Cost)
	{
		return;
	}
	else
	{
		currentCost = Cost;
		isOnBuild = true;
		Tower = newTower;
	}
}


void AMyplayer::CanBuild()
{
	FHitResult hit;
	FVector cameraLoc = UGameplayStatics::GetPlayerCameraManager(GetWorld(),0)->GetCameraLocation();
	APlayerController* playerController = Cast<APlayerController>(GetController());
	playerController->GetHitResultUnderCursor(DetectionObjectQuery, false, hit);
	FVector MousePos = hit.Location;
	TArray<AActor*> ActorToIgnore;
	ActorToIgnore.Add(this);
	FHitResult TraceHit;

	bool validhit = UKismetSystemLibrary::LineTraceSingle(GetWorld(), cameraLoc, MousePos, TraceChannel, false, ActorToIgnore, EDrawDebugTrace::Type::ForOneFrame, TraceHit, true, FLinearColor::Red, FLinearColor::Green,2.0f);
	if (validhit == false)
	{
		isOnValidLoc = false;
		return;
	}
	hitResult = TraceHit;
	tempStorage = TraceHit.Location;
	
	if (BuildTarget != NULL)
	{
		return;
	}
	ATower* towers = GetWorld()->SpawnActor<ATower>(Tower, TraceHit.Location, FRotator(0, 0, 0));
	towers->SetActorTickEnabled(false);
	BuildTarget = towers;

}

void AMyplayer::ghostTower()
{
	
	if (BuildTarget == NULL)
	{
		return;
	}

	ATowerHolder* holder = Cast<ATowerHolder>(hitResult.Actor);
	if (holder == NULL)
	{
		
		
		BuildTarget->SetActorLocation(tempStorage);
		isOnValidLoc = false;
	}
	else
	{
		isOnValidLoc = true;
		BuildTarget->SetActorLocation(holder->TowerLocation());
	}

}

void AMyplayer::PlaceTower()
{
	isOnBuild = false;
	if (BuildTarget == NULL|| isOnValidLoc== false)
	{
		if (BuildTarget != NULL)
		{
			BuildTarget->Destroy();
			BuildTarget = NULL;
		}
		return;
	}
	ATowerHolder* holder = Cast<ATowerHolder>(hitResult.Actor);
	BuildTarget->SetActorLocation(holder->TowerLocation());

	BuildTarget->SetActorTickEnabled(true);
	BuildTarget = NULL;
	
	playerMoney = playerMoney - currentCost;

}

