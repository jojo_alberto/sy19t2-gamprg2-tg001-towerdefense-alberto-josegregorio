// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyAIController.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Enemy.h"


void AEnemyAIController::waiterTrueer()
{
	waiter = true;
}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);
	if (waiter == false)
	{
		return;
	}
	waiter = false;
	GetWorld()->GetTimerManager().ClearTimer(hitTimer);
	GetWorld()->GetTimerManager().SetTimer(hitTimer, this, &AEnemyAIController::waiterTrueer, 0.1f, false);
	AEnemy* EnemyAI = Cast<AEnemy>(GetPawn());

	if (EnemyAI)
	{
		EnemyAI->MoveToWaypoint();
	}
}
