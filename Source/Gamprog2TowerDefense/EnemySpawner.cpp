// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemySpawner.h"
#include "HP.h"
#include "TimerManager.h"
#include "Engine.h"
#include "Myplayer.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/TargetPoint.h"

// Sets default values
AEnemySpawner::AEnemySpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AEnemySpawner::spawEnemyWave()
{
	if (enemyWaveIndex >= enemy->enemyWave.Num())
	{
		enemyWaveIndex = 0;
		elementIntdex = 0;
		return;
	}
	spawnEnemy(enemy->enemyWave[enemyWaveIndex]);
//	int32 totalEnemy = 0;
//
//	for (auto i : enemy->enemyWave)
//	{
//		totalEnemy += i.Count;
//		spawnEnemy(i);
//		
//	}
//	EnemyRemaining = totalEnemy;
}


void AEnemySpawner::spawnEnemy(FenemyWave d_enemy)
{
	if (GetWorld() == NULL)
	{
		return;
	}
	
	/*for (int32 i = 0; i < d_enemy.Count; i++)
	{*/		
		//GetWorld()->SpawnActor<AEnemy>(d_enemy.EnemyType, spawnPoint->GetActorLocation(), spawnPoint->GetActorRotation());
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AEnemy * Enemy = GetWorld()->SpawnActor<AEnemy>(d_enemy.EnemyType, spawnPoint->GetActorLocation(), spawnPoint->GetActorRotation(),SpawnInfo);
		
		//if (Enemy == NULL)
		//{
		//	enemyWaveIndex = 0;
		//	elementIntdex = 0;
		//	return;
		//	//GEngine->AddOnScreenDebugMessage(-1, 5.0f,FColor::Red,"test");
		//	//continue;
		//}
		Enemy->Waypoints = TArray<AActor*>(waypointer);
		Enemy->currrentWayPoint = 0;
		Enemy->MoveToWaypoint();
		Enemy->EnemyHealth->CharDie.AddDynamic(this, &AEnemySpawner::DecrementEnemyCount);
		
		EnemyRemaining++;
	
		

		GetWorld()->GetTimerManager().SetTimer(timer, this, &AEnemySpawner::SpawnSingleEnemy, 0.3, false);
	/*}*/
	
}

// Called when the game starts or when spawned
void AEnemySpawner::BeginPlay()
{
	Super::BeginPlay();
	
}
void AEnemySpawner::DecrementEnemyCount()
{
	EnemyRemaining--;
	if (EnemyRemaining <= 0)
	{
		AMyplayer* player = Cast<AMyplayer>(UGameplayStatics::GetPlayerPawn(GetWorld(),0));
		player->playerMoney += reward;

	}
}
// Called every frame
void AEnemySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemySpawner::SpawnSingleEnemy()
{
	if (elementIntdex >= enemy->enemyWave[enemyWaveIndex].Count)
	{
		enemyWaveIndex++;
	}
	if (enemyWaveIndex >= enemy->enemyWave.Num())
	{
		enemyWaveIndex = 0;
		elementIntdex = 0;
		return;
	}
	if (enemyWaveIndex + 1 < enemy->enemyWave.Num())
	{

		spawEnemyWave();
		return; // (,Y,)
				//	Y Y
				//  \ /
	}
	elementIntdex++;
	GetWorld()->GetTimerManager().ClearTimer(timer);
	spawnEnemy(enemy->enemyWave[enemyWaveIndex]);
}



