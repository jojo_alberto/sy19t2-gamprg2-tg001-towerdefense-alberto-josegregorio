// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class GAMPROG2TOWERDEFENSE_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
public:
	FTimerHandle hitTimer;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool waiter = true;
	UFUNCTION()
	void waiterTrueer();
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
};
