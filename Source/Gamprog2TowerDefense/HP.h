// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HP.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDie);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPROG2TOWERDEFENSE_API UHP : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHP();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float currentHP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float maxHP;
	UFUNCTION(BlueprintCallable)
	void TakeDamage(int32 damage);
	UPROPERTY(BlueprintAssignable)
	FOnDie CharDie;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
