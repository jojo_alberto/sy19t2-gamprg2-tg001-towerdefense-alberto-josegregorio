// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WaveDataAsset.h"
#include "EnemySpawner.generated.h"


UCLASS()
class GAMPROG2TOWERDEFENSE_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawner();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWaveDataAsset* enemy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class ATargetPoint* spawnPoint;
	UFUNCTION(BlueprintCallable,Category="EnemySpawner")
	void spawEnemyWave();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float spawnDelay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AActor*> waypointer;

	UFUNCTION(BlueprintCallable)
	void spawnEnemy(FenemyWave d_enemy);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 EnemyRemaining = 0;
	UFUNCTION(BlueprintCallable)
	void DecrementEnemyCount();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float reward;
	FTimerHandle timer;
	UFUNCTION(BlueprintCallable)
	void SpawnSingleEnemy();
	int32 enemyWaveIndex = 0;
	int32 elementIntdex = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
