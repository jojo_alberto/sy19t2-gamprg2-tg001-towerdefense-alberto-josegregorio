// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Enemy.h"
#include "GameModeBaseClass.generated.h"

/**
 * 
 */
UCLASS()
class GAMPROG2TOWERDEFENSE_API AGameModeBaseClass : public AGameModeBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int HP;


protected:
	//void SpawnEnemy();
};
