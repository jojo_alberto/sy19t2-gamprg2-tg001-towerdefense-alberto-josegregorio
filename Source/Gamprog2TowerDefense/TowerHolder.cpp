// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerHolder.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SceneComponent.h"

// Sets default values
ATowerHolder::ATowerHolder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//TowerLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("TowerLocation"));
	//BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("TowerLocation"));
	////SetRootComponent(TowerLocation);
	//TowerLocation->SetupAttachment(RootComponent);
	//BoxCollision->SetupAttachment(TowerLocation);

}

// Called when the game starts or when spawned
void ATowerHolder::BeginPlay()
{
	Super::BeginPlay();
	
}

FVector ATowerHolder::TowerLocation_Implementation()
{
	return GetActorLocation();
}

// Called every frame
void ATowerHolder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

