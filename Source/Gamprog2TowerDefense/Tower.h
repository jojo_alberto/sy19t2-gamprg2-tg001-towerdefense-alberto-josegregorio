// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ammo.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Tower.generated.h"

UCLASS()
class GAMPROG2TOWERDEFENSE_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AAmmo>  Projectile;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RadiusDetection;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TEnumAsByte<EObjectTypeQuery>> DetectionObjectQuery;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AEnemy* currentTarget;
	UFUNCTION(BlueprintCallable)
	void Attack();
	UFUNCTION(BlueprintCallable)
	void DetectEnemy();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AttackRate;
	UFUNCTION(BlueprintCallable)
	void attackCD();
	FTimerHandle AttackRateHandle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool canAttack = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float towerCost;



};
