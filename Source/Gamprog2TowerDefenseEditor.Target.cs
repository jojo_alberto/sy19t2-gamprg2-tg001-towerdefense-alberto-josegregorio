// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Gamprog2TowerDefenseEditorTarget : TargetRules
{
	public Gamprog2TowerDefenseEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "Gamprog2TowerDefense" } );
	}
}
